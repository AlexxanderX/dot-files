local config = {
  "romgrk/barbar.nvim",
  dependencies = {
    "nvim-tree/nvim-web-devicons"
  },
  keys = {
    { "<Leader>d", ":Bdelete<CR>", desc = "Delete current buffer", silent = true },
    { "<Leader>[", "<Cmd>BufferPrevious<CR>", desc = "Switch to previous buffer", silent = true, noremap = true },
    { "<Leader>]", "<Cmd>BufferNext<CR>", desc = "Switch to next buffer", silent = true, noremap = true }
  },
  init = function() 
    vim.g.barbar_auto_setup = false
    local opts = { noremap = true, silent = true }
  end,
  opts = {
  }
}

return config

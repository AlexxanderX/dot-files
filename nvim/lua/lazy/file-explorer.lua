return {
  "tamago324/lir.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "kyazdani42/nvim-web-devicons"
  },
  keys = {
    { "<space>e", ":lua require('lir.float').toggle()<CR>", desc = "Toggle file-explorer", silent = true }
  },
  lazy = true,
  init = function()
    local actions = require("lir.actions")
    local mark_actions = require("lir.mark.actions")
    local clipboard_actions = require("lir.clipboard.actions")

    require("lir").setup({
      show_hidden_files = true,
      ignore = {},
      devicons = {
        enable = true,
	    highlight_dirname = false
      },
      hide_cursor = true,
      float = {
        winblend = 0,
        curdir_window = {
          enable = false,
          highlight_dirname = false
        }
      },
      mappings = {
        ['l']  = actions.edit,
        ['<CR>']  = actions.edit,

        ['<C-s>'] = actions.split,
        ['<C-v>'] = actions.vsplit,
        ['<C-t>'] = actions.tabedit,

        ['u']     = actions.up,
        ['q']     = actions.quit,

        ['A']     = actions.mkdir,
        ['a']     = actions.newfile,
        ['R']     = actions.rename,
        ['@']     = actions.cd,
        ['Y']     = actions.yank_path,
        ['.']     = actions.toggle_show_hidden,
        ['D']     = actions.delete,

        ['J'] = function()
          mark_actions.toggle_mark()
          vim.cmd('normal! j')
        end,
        ['C'] = clipboard_actions.copy,
        ['X'] = clipboard_actions.cut,
        ['P'] = clipboard_actions.paste,
      }
    })
  end
}

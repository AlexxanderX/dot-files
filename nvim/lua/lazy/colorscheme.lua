local config = {
  "rebelot/kanagawa.nvim",
  name = "kanagawa",
  lazy = false,
  priority = 1000,
  config = function()
    -- load the colorscheme here
    vim.cmd([[colorscheme kanagawa]])
  end,
}

return config

local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- Install your plugins here
return packer.startup(function(use)
	-- My plugins here
	use({ "wbthomason/packer.nvim", commit = "de109156cfa634ce0256ea4b6a7c32f9186e2f10" }) -- Have packer manage itself
	use({ "nvim-lua/plenary.nvim", commit = "986ad71ae930c7d96e812734540511b4ca838aa2" }) -- Useful lua functions used by lots of plugins
	-- use({ "windwp/nvim-autopairs", commit = "972a7977e759733dd6721af7bcda7a67e40c010e" }) -- Autopairs, integrates with both cmp and treesitter
	-- use({ "numToStr/Comment.nvim", commit = "78ab4e9785b6da9b7a539df3bd6f70300dc9482b" }) -- Comments
	-- use({ "JoosepAlviste/nvim-ts-context-commentstring", commit = "88343753dbe81c227a1c1fd2c8d764afb8d36269" }) -- Settings for comments
	use({ "kyazdani42/nvim-web-devicons", commit = "2d02a56189e2bde11edd4712fea16f08a6656944" }) -- DevIcons
	use({ "kyazdani42/nvim-tree.lua", commit = "011a7816b8ea1b3697687a26804535f24ece70ec" }) -- File explorer
	use({ "akinsho/bufferline.nvim", commit = "fb7b17362eb6eedc57c37bdfd364f8e7d8149e31" }) -- Tabs
	use({ "moll/vim-bbye", commit = "25ef93ac5a87526111f43e5110675032dbcacf56" }) -- For closing the current buffer without closing the split view and automatically switching to another buffer
	use({ "nvim-lualine/lualine.nvim", commit = "5f68f070e4f7158517afc55f125a6f5ed1f7db47" }) -- Statusline
	-- use({ "akinsho/toggleterm.nvim", commit = "aaeed9e02167c5e8f00f25156895a6fd95403af8" }) -- Interesting idea
	-- use({ "ahmedkhalf/project.nvim", commit = "541115e762764bc44d7d3bf501b6e367842d3d4f" }) -- Interesting idea
	use({ "lewis6991/impatient.nvim", commit = "2aa872de40dbbebe8e2d3a0b8c5651b81fe8b235" }) -- Optimizations to plugins load time
	use({ "lukas-reineke/indent-blankline.nvim", commit = "c15bbe9f23d88b5c0b4ca45a446e01a0a3913707" }) -- Show indentations
	-- use({ "goolord/alpha-nvim", commit = "ef27a59e5b4d7b1c2fe1950da3fe5b1c5f3b4c94" }) -- Interesting idea
	use({ "folke/which-key.nvim", commit = "bd4411a2ed4dd8bb69c125e339d837028a6eea71" }) -- Show possible commands when typing in COMMAND mode
    -- use({ "RRethy/vim-illuminate", commit = "6bfa5dc069bd4aa8513a3640d0b73392094749be" }) -- Highlight intelligently
    use({ "junegunn/fzf" })
    use({ "junegunn/fzf.vim" })
    use({ "kkoomen/vim-doge", run = ":call doge#install({ 'headless': 1 })" }) -- Documentation generator
    use({ "godlygeek/tabular" })
    use({ "max397574/colortils.nvim", config = function() require("colortils").setup() end }) -- Colors wheel
    use({ "preservim/vim-markdown", commit = "3a9643961233c2812816078af8bd1eaabc530dce" }) -- Markdown
    vim.cmd("set grepprg=rg\\ --vimgrep\\ --smart-case\\ --follow")

	-- Colorschemes
  use('ellisonleao/gruvbox.nvim')

	-- cmp plugins
	use({ "hrsh7th/nvim-cmp", commit = "706371f1300e7c0acb98b346f80dad2dd9b5f679" }) -- The completion plugin
	use({ "hrsh7th/cmp-buffer", commit = "62fc67a2b0205136bc3e312664624ba2ab4a9323" }) -- buffer completions
	use({ "hrsh7th/cmp-path", commit = "447c87cdd6e6d6a1d2488b1d43108bfa217f56e1" }) -- path completions
	use({ "saadparwaiz1/cmp_luasnip", commit = "a9de941bcbda508d0a45d28ae366bb3f08db2e36" }) -- snippet completions
	use({ "hrsh7th/cmp-nvim-lsp", commit = "affe808a5c56b71630f17aa7c38e15c59fd648a8" }) -- nvim-cmp source for neovim's built-in language server client
	use({ "hrsh7th/cmp-nvim-lua", commit = "d276254e7198ab7d00f117e88e223b4bd8c02d21" }) -- nvim-cmp source for neovim Lua API

	-- snippets
	use({ "L3MON4D3/LuaSnip", commit = "53e812a6f51c9d567c98215733100f0169bcc20a" }) -- snippet engine
	-- use({ "rafamadriz/friendly-snippets", commit = "d27a83a363e61009278b6598703a763ce9c8e617" }) -- a bunch of snippets to use

	-- LSP
	use({ "neovim/nvim-lspconfig", commit = "636ce36c30725391486377850bf8460dc0723ae2" }) -- enable LSP
	use({ "williamboman/nvim-lsp-installer", commit = "ae913cb4fd62d7a84fb1582e11f2e15b4d597123" }) -- simple to use language server installer
	use({ "jose-elias-alvarez/null-ls.nvim", commit = "753ad51790a966b42997ac935e26573fb6d5864a" }) -- for formatters and linters

	-- Telescope
	use({ "nvim-telescope/telescope.nvim", commit = "49b043e2a3e63cdd50bcde752e3b32dae22d8a3a" })

    --use({ "folke/trouble.nvim", commit = "da61737d860ddc12f78e638152834487eabf0ee5" }) -- Show diagnostics at bottom

	-- Treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		commit = "36ee4890c47a9de5789d6561b19ce36da8b766be",
	})

	-- Git
	-- use({ "lewis6991/gitsigns.nvim", commit = "c18e016864c92ecf9775abea1baaa161c28082c3" })

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)

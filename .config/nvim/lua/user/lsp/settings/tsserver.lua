return {
	capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities()),
	on_attach = function(client)
		client.resolved_capabilities.document_formatting = false
	end,
	root_dir = require("lspconfig.util").root_pattern(".git"),
	init_options = {
        hostInfo = "neovim",
	},
    settings = {
        javascript = {
            suggest = {
                autoImports = false
            },
        },
        typescript = {
            suggest = {
                autoImports = false
            },
            tsserver = {
                maxTsServerMemory = 6144
            }
        }
    }
}

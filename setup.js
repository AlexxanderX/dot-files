const fs = require('fs/promises');
const path = require('path');
const os = require('os');

const OSType = {
  Linux: "0",
  Windows: "1"
};

const DistroType = {
  Arch: "0"
};

const DEType = {
  XfceGTK: "0"
};

const ToolType = {
  NeoVim: "01"
};

function printName() {
  console.log(" Created by Alexandru Prisacariu");
  console.log("           _           _____      _                          _       ");
  console.log("     /\\   | |         |  __ \\    (_)                        (_)      ");
  console.log("    /  \\  | | _____  _| |__) | __ _ ___  __ _  ___ __ _ _ __ _ _   _ ");
  console.log("   / /\\ \\ | |/ _ \\ \\/ /  ___/ '__| / __|/ _` |/ __/ _` | '__| | | | |");
  console.log("  / ____ \\| |  __/>  <| |   | |  | \\__ \\ (_| | (_| (_| | |  | | |_| |");
  console.log(" /_/    \\_\\_|\\___/_/\\_\\_|   |_|  |_|___/\\__,_|\\___\\__,_|_|  |_|\\__,_|");
  console.log("                                                                     ");
  console.log("                                                                     ");
}

function printSeparator() {
  console.log("================================================================================")
}

function printFirstDigit() {
  console.log("OS / First digit:");
  console.log(`\t${OSType.Linux}.Linux`);
  console.log(`\t${OSType.Windows}.Windows`);
}

function printSecondDigit() {
  console.log(`Distro / Second digit ("-" if Windows):`);
  console.log(`\t${DistroType.Arch}.Arch/EndeavourOS`);
}

function printThirdDigit() {
  console.log(`Desktop environment / Third digit ("-" if Windows):`);
  console.log(`\t${DEType.XfceGTK}.Xfce (GTK)`);
}

function printRestDigits() {
  console.log(`Tools configs to install / Rest of the digits:`);
  console.log(`\t${ToolType.NeoVim}.NeoVim`);
}

async function run() {
  printName();

  const lastArgument = process.argv[process.argv.length - 1];
  if (lastArgument[0] !== "0" && lastArgument[0] !== "1") {
    console.log("Write as command line argument to the script the following.");
    printFirstDigit();
    printSeparator();
    printSecondDigit();
    printSeparator();
    printThirdDigit();
    printSeparator();
    printRestDigits();
    console.log("");
  } else {
    const OS = lastArgument[0];
    const Distro = lastArgument[1];
    const DE = lastArgument[2];
    const Tools = [];

    {
      const RestDigits = lastArgument.substring(3);
      for (let i=0, length=RestDigits.length; i < length; i += 2) {
        Tools.push(RestDigits.substring(i, 2));
      }
    }

    if (OS === OSType.Linux) {
      if (Distro === DistroType.Arch) {
      }

      if (DE === DEType.XfceGTK) {
        {
          const gtkCSSFilePath = path.join(".config", "gtk-3.0", "gtk.css");
          const fromPath = path.join(__dirname, "linux", "gtk3", gtkCSSFilePath);
          const toPath = path.join(os.homedir(), gtkCSSFilePath);
          await fs.writeFile(toPath, await fs.readFile(fromPath, "utf-8"));
          await fs.chmod(toPath, 0o0755);
          console.log(`Writing GTK3.0 CSS file path from "${fromPath}" to "${toPath}" (0o0755).`);
        }
      }

      if (Tools.includes(ToolType.NeoVim)) {
      }
    }
  }
}

run();

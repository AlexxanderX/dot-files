#!/bin/sh
sudo apt install zsh -y
zsh --version

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Copy config
cp ./.zshrc ~/.zshrc
